# README #

In this application, you will be able to get information about sensors by activating the Service.


After activating it, you will find below all the sensors available and a color linked to each sensor (yellow if the sensor has detected light, and grey if not).


If you want the service to start at boot, you will find a checkbox in the settings to activate it.


In the settings, you will also be able to enter your information (username and mail address).


The application will send you a notification between 19h and 23h during the week if a sensor detects light, telling you which sensor detected it.