package eu.telecomnancy.iotlab.presenter;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Damien on 2/2/2016.
 */
public interface MainPresenter {

    public void manageService(Context context);
    public void onDestroy(Context context);

}
