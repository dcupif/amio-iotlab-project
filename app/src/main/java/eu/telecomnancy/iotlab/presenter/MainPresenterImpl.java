package eu.telecomnancy.iotlab.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import eu.telecomnancy.iotlab.model.MainService;
import eu.telecomnancy.iotlab.model.ServiceStatus;
import eu.telecomnancy.iotlab.view.MainActivity;
import eu.telecomnancy.iotlab.view.MainView;

/**
 * Created by Damien on 2/2/2016.
 */
public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;

    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
    }

    public void manageService(Context context) {
        if (MainService.status == ServiceStatus.STOPPED) {
            context.startService(new Intent(context, eu.telecomnancy.iotlab.model.MainService.class));
        } else {
            context.stopService(new Intent(context, eu.telecomnancy.iotlab.model.MainService.class));
        }
    }

    public void onDestroy(Context context) {
        context.stopService(new Intent(context, eu.telecomnancy.iotlab.model.MainService.class));
        mainView = null;
    }

}
