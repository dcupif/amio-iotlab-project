package eu.telecomnancy.iotlab.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Switch;

import eu.telecomnancy.iotlab.R;
import eu.telecomnancy.iotlab.presenter.MainPresenter;
import eu.telecomnancy.iotlab.presenter.MainPresenterImpl;

public class MainActivity extends AppCompatActivity implements MainView {

    private MainPresenter presenter;

    private Switch serviceSwitch;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenterImpl(this);

        // get graphical elements
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        serviceSwitch = (Switch) findViewById(R.id.service_switch);

        // set graphical elements configuration
        setSupportActionBar(toolbar);

        // set graphical elements listeners
        serviceSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.manageService(MainActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy(this);
        super.onDestroy();
    }
}
