package eu.telecomnancy.iotlab.model;

/**
 * Created by Damien on 2/3/2016.
 */
public enum ServiceStatus {
    RUNNING,
    STOPPED;
}
