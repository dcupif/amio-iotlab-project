package eu.telecomnancy.iotlab.model;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MainService extends Service {

    public static ServiceStatus status = ServiceStatus.STOPPED;

    @Override
    public void onCreate() {
        super.onCreate();
        status = ServiceStatus.RUNNING;
        Log.d("Service", "Launched.");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        status = ServiceStatus.STOPPED;
        Log.d("Service", "Terminated.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
